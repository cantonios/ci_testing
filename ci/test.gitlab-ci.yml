test:linux:vars:
  stage: test
  image: ubuntu:18.04
  script:
    - export
  rules:
    - if: '$CI_PIPELINE_SOURCE != "external"'

.test:linux:base:
  stage: test
  image: ubuntu:18.04
  before_script:
    # Set noninteractive, otherwise tzdata may be installed and prompt for a
    # geographical region.
    - export DEBIAN_FRONTEND=noninteractive
    - apt-get update -y
    # Compilers need to be installed to ensure the correct libraries versions
    # (e.g. glibc) are available.
    - apt-get install -y --no-install-recommends software-properties-common
    - add-apt-repository -y  ppa:ubuntu-toolchain-r/test
    - apt-get update
    - apt-get install --no-install-recommends -y ${EIGEN_CI_CXX_COMPILER}
      ${EIGEN_CI_CC_COMPILER} cmake ninja-build
    # ONLY FOR CI TESTING: clone into current directory
    - git clone --depth 1 --branch ${EIGEN_CI_GIT_BRANCH} ${EIGEN_CI_GIT_URL} eigen_repo && cp -R eigen_repo/* .
  script:
    - export NPROC=`nproc`
    - echo ${NPROC}
    - cd ${EIGEN_CI_BUILDDIR}
    # Re-run failures once, which is much faster than retrying the entire job.
    # Note that we do not update the CDash results (i.e. no -T test) on re-run.
    - ctest -j${NPROC} --output-on-failure --no-compress-output
        --build-no-clean -T test ${EIGEN_CI_TEST_TARGET} ||
      ctest -j${NPROC} --output-on-failure --no-compress-output --rerun-failed
  after_script:
    - apt-get update -y
    - apt-get install --no-install-recommends -y xsltproc
    - cd ${EIGEN_CI_BUILDDIR}
    - xsltproc ../ci/CTest2JUnit.xsl Testing/`head -n 1 < Testing/TAG`/Test.xml > "JUnitTestResults_$CI_JOB_ID.xml"
  artifacts:
    reports:
      junit:
        - ${EIGEN_CI_BUILDDIR}/JUnitTestResults_$CI_JOB_ID.xml
    expire_in: 5 days
  only:
    - schedules
    - web

.test:windows:base:
  stage: test
  before_script:
    # ONLY FOR CI TESTING: clone into current directory
    - git clone --depth 1 --branch ${EIGEN_CI_GIT_BRANCH} ${EIGEN_CI_GIT_URL} eigen_repo &&
      robocopy eigen_repo . /s /e > robocopy.log || echo 'copied files.'
  script:
    # Inline batch script launched by powershell.
    - cd $EIGEN_CI_BUILDDIR
    - $NPROC=${Env:NUMBER_OF_PROCESSORS}
    # We need to split EIGEN_CI_TEST_TARGET, otherwise it is interpretted
    # as a single argument.  Split by space, unless double-quoted.
    - $split_target = [regex]::Split(${EIGEN_CI_TEST_TARGET}, ' (?=(?:[^"]|"[^"]*")*$)' )
    # Re-run failures once, which is much faster than retrying the entire job.
    # Note that we do not update the CDash results (i.e. no -T test) on re-run.
    - ctest -j$NPROC --output-on-failure --no-compress-output --build-no-clean -T test ${split_target} ||
      ctest -j$NPROC --output-on-failure --no-compress-output --rerun-failed
  after_script:   
    - cd ${EIGEN_CI_BUILDDIR}
    - $TEST_TAG = Get-Content Testing\TAG | select -first 1
    # PowerShell equivalent to xsltproc:
    - $XSL_FILE = Resolve-Path "..\ci\CTest2JUnit.xsl"
    - $INPUT_FILE = Resolve-Path Testing\$TEST_TAG\Test.xml
    - $OUTPUT_FILE = Join-Path -Path $pwd -ChildPath JUnitTestResults_$CI_JOB_ID.xml
    - $xslt = New-Object System.Xml.Xsl.XslCompiledTransform;
    - $xslt.Load($XSL_FILE)
    - $xslt.Transform($INPUT_FILE,$OUTPUT_FILE)
  artifacts:
    reports:
      junit:
        - ${EIGEN_CI_BUILDDIR}/JUnitTestResults_${CI_JOB_ID}.xml
    expire_in: 5 days
  only:
    - schedules
    - web

##### x86-64 ###################################################################
.test:x86-64:linux:base:
  extends: .test:linux:base
  tags:
    - eigen-runner
    - linux
    - x86-64

# GCC-5 (minimum for c++14)
.test:x86-64:linux:gcc-5:default:
  extends: .test:x86-64:linux:base
  needs: [ "build:x86-64:linux:gcc-5:default" ]
  variables:
    EIGEN_CI_CXX_COMPILER: "g++-5"
    EIGEN_CI_CC_COMPILER: "gcc-5"

test:x86-64:linux:gcc-5:default:official:
  extends: .test:x86-64:linux:gcc-5:default
  variables:
    EIGEN_CI_TEST_TARGET: -L "Official"

test:x86-64:linux:gcc-5:default:unsupported:
  extends: .test:x86-64:linux:gcc-5:default
  variables:
    EIGEN_CI_TEST_TARGET: -L "Unsupported"
    
# GCC-9 (default on Ubuntu 20.04 LTS)
.test:x86-64:linux:gcc-9:default:
  extends: .test:x86-64:linux:base
  needs: [ "build:x86-64:linux:gcc-9:default" ]
  variables:
    EIGEN_CI_CXX_COMPILER: "g++-9"
    EIGEN_CI_CC_COMPILER: "gcc-9"

test:x86-64:linux:gcc-9:default:official:
  extends: .test:x86-64:linux:gcc-9:default
  variables:
    EIGEN_CI_TEST_TARGET: -L "Official"

test:x86-64:linux:gcc-9:default:unsupported:
  extends: .test:x86-64:linux:gcc-9:default
  variables:
    EIGEN_CI_TEST_TARGET: -L "Unsupported"

# GCC-10
.test:x86-64:linux:gcc-10:default:
  extends: .test:x86-64:linux:base
  needs: [ "build:x86-64:linux:gcc-10:default" ]
  variables:
    EIGEN_CI_CXX_COMPILER: "g++-10"
    EIGEN_CI_CC_COMPILER: "gcc-10"

test:x86-64:linux:gcc-10:default:official:
  extends: .test:x86-64:linux:gcc-10:default
  variables:
    EIGEN_CI_TEST_TARGET: -L "Official"

test:x86-64:linux:gcc-10:default:unsupported:
  extends: .test:x86-64:linux:gcc-10:default
  variables:
    EIGEN_CI_TEST_TARGET: -L "Unsupported"

.test:x86-64:linux:gcc-10:avx2:
  extends: .test:x86-64:linux:base
  needs: [ "build:x86-64:linux:gcc-10:avx2" ]
  variables:
    EIGEN_CI_CXX_COMPILER: "g++-10"
    EIGEN_CI_CC_COMPILER: "gcc-10"

test:x86-64:linux:gcc-10:avx2:official:
  extends: .test:x86-64:linux:gcc-10:avx2
  variables:
    EIGEN_CI_TEST_TARGET: -L "Official"

test:x86-64:linux:gcc-10:avx2:unsupported:
  extends: .test:x86-64:linux:gcc-10:avx2
  variables:
    EIGEN_CI_TEST_TARGET: -L "Unsupported"

.test:x86-64:linux:gcc-10:avx512dq:
  extends: .test:x86-64:linux:base
  needs: [ "build:x86-64:linux:gcc-10:avx512dq" ]
  variables:
    EIGEN_CI_CXX_COMPILER: "g++-10"
    EIGEN_CI_CC_COMPILER: "gcc-10"

test:x86-64:linux:gcc-10:avx512dq:official:
  extends: .test:x86-64:linux:gcc-10:avx512dq
  variables:
    EIGEN_CI_TEST_TARGET: -L "Official"

test:x86-64:linux:gcc-10:avx512dq:unsupported:
  extends: .test:x86-64:linux:gcc-10:avx512dq
  variables:
    EIGEN_CI_TEST_TARGET: -L "Unsupported"

# Clang-6 (minimum installable on Ubuntu 20.04 LTS)
.test:x86-64:linux:clang-6:default:
  extends: .test:x86-64:linux:base
  needs: [ "build:x86-64:linux:clang-6:default" ]
  variables:
    EIGEN_CI_CXX_COMPILER: "clang++-6.0"
    EIGEN_CI_CC_COMPILER: "clang-6.0"

test:x86-64:linux:clang-6:default:official:
  extends: .test:x86-64:linux:clang-6:default
  variables:
    EIGEN_CI_TEST_TARGET: -L "Official"

test:x86-64:linux:clang-6:default:unsupported:
  extends: .test:x86-64:linux:clang-6:default
  variables:
    EIGEN_CI_TEST_TARGET: -L "Unsupported"
    
# Clang-9 (default on Ubuntu 20.04 LTS)
.test:x86-64:linux:clang-9:default:
  extends: .test:x86-64:linux:base
  needs: [ "build:x86-64:linux:clang-9:default" ]
  variables:
    EIGEN_CI_CXX_COMPILER: "clang++-9"
    EIGEN_CI_CC_COMPILER: "clang-9"

test:x86-64:linux:clang-9:default:official:
  extends: .test:x86-64:linux:clang-9:default
  variables:
    EIGEN_CI_TEST_TARGET: -L "Official"

test:x86-64:linux:clang-9:default:unsupported:
  extends: .test:x86-64:linux:clang-9:default
  variables:
    EIGEN_CI_TEST_TARGET: -L "Unsupported"

# Clang-12 (latest release)
.test:x86-64:linux:clang-12:default:
  extends: .test:x86-64:linux:base
  image: ubuntu:20.04
  needs: [ "build:x86-64:linux:clang-12:default" ]
  variables:
    EIGEN_CI_CXX_COMPILER: "clang++-12"
    EIGEN_CI_CC_COMPILER: "clang-12"

test:x86-64:linux:clang-12:default:official:
  extends: .test:x86-64:linux:clang-12:default
  variables:
    EIGEN_CI_TEST_TARGET: -L "Official"

test:x86-64:linux:clang-12:default:unsupported:
  extends: .test:x86-64:linux:clang-12:default
  variables:
    EIGEN_CI_TEST_TARGET: -L "Unsupported"

.test:x86-64:linux:clang-12:avx2:
  extends: .test:x86-64:linux:base
  image: ubuntu:20.04
  needs: [ "build:x86-64:linux:clang-12:avx2" ]
  variables:
    EIGEN_CI_CXX_COMPILER: "clang++-12"
    EIGEN_CI_CC_COMPILER: "clang-12"

test:x86-64:linux:clang-12:avx2:official:
  extends: .test:x86-64:linux:clang-12:avx2
  variables:
    EIGEN_CI_TEST_TARGET: -L "Official"

test:x86-64:linux:clang-12:avx2:unsupported:
  extends: .test:x86-64:linux:clang-12:avx2
  variables:
    EIGEN_CI_TEST_TARGET: -L "Unsupported"

.test:x86-64:linux:clang-12:avx512dq:
  extends: .test:x86-64:linux:base
  image: ubuntu:20.04
  needs: [ "build:x86-64:linux:clang-12:avx512dq" ]
  variables:
    EIGEN_CI_CXX_COMPILER: "clang++-12"
    EIGEN_CI_CC_COMPILER: "clang-12"
  tags:
    - eigen-runner
    - linux
    - x86-64
    - avx512

test:x86-64:linux:clang-12:avx512dq:official:
  extends: .test:x86-64:linux:clang-12:avx512dq
  variables:
    EIGEN_CI_TEST_TARGET: -L "Official"

test:x86-64:linux:clang-12:avx512dq:unsupported:
  extends: .test:x86-64:linux:clang-12:avx512dq
  variables:
    EIGEN_CI_TEST_TARGET: -L "Unsupported"

.test:x86-64:linux:clang-12:default:asan:
  extends: .test:x86-64:linux:clang-12:default
  needs: [ "build:x86-64:linux:clang-12:default:asan" ]

test:x86-64:linux:clang-12:default:asan:official:
  extends: .test:x86-64:linux:clang-12:default:asan
  variables:
    EIGEN_CI_TEST_TARGET: -L "Official"

test:x86-64:linux:clang-12:default:asan:unsupported:
  extends: .test:x86-64:linux:clang-12:default:asan
  variables:
    EIGEN_CI_TEST_TARGET: -L "Unsupported"
    
# .test:x86-64:linux:clang-12:default:msan:
#   extends: .test:x86-64:linux:clang-12:default
#   needs: [ "build:x86-64:linux:clang-12:default:msan" ]

# test:x86-64:linux:clang-12:default:msan:official:
#   extends: .test:x86-64:linux:clang-12:default:msan
#   variables:
#     EIGEN_CI_TEST_TARGET: -L "Official"

# test:x86-64:linux:clang-12:default:msan:unsupported:
#   extends: .test:x86-64:linux:clang-12:default:msan
#   variables:
#     EIGEN_CI_TEST_TARGET: -L "Unsupported"

##### CUDA #####################################################################
.test:x86-64:linux:cuda:base:
  extends: .test:linux:base
  allow_failure: true
  variables:
    EIGEN_CI_TEST_TARGET: -L "gpu"
  tags: 
    - eigen-runner
    - linux
    - x86-64
    - cuda

# GCC-7, CUDA-9.2
test:x86-64:linux:gcc-7:cuda-9.2:
  extends: .test:x86-64:linux:cuda:base
  image: nvidia/cuda:9.2-devel-ubuntu18.04
  needs: [ "build:x86-64:linux:gcc-7:cuda-9.2" ]
  variables:
    EIGEN_CI_CXX_COMPILER: "g++-7"
    EIGEN_CI_CC_COMPILER: "gcc-7"

# Clang-10, CUDA-9.2
test:x86-64:linux:clang-10:cuda-9.2:
  extends: .test:x86-64:linux:cuda:base
  image: nvidia/cuda:9.2-devel-ubuntu18.04
  needs: [ "build:x86-64:linux:clang-10:cuda-9.2" ]
  variables:
    EIGEN_CI_CXX_COMPILER: "clang++-10"
    EIGEN_CI_CC_COMPILER: "clang-10"

# GCC-8, CUDA-10.2
test:x86-64:linux:gcc-8:cuda-10.2:
  extends: .test:x86-64:linux:cuda:base
  image: nvidia/cuda:10.2-devel-ubuntu18.04
  needs: [ "build:x86-64:linux:gcc-8:cuda-10.2" ]
  variables:
    EIGEN_CI_CXX_COMPILER: "g++-8"
    EIGEN_CI_CC_COMPILER: "gcc-8"

# Clang-10, CUDA-10.2
test:x86-64:linux:clang-10:cuda-10.2:
  extends: .test:x86-64:linux:cuda:base
  image: nvidia/cuda:10.2-devel-ubuntu18.04
  needs: [ "build:x86-64:linux:clang-10:cuda-10.2" ]
  variables:
    EIGEN_CI_CXX_COMPILER: "clang++-10"
    EIGEN_CI_CC_COMPILER: "clang-10"

# GCC-10, CUDA-11.4 
test:x86-64:linux:gcc-10:cuda-11.4:
  extends: .test:x86-64:linux:cuda:base
  image: nvidia/cuda:11.4.2-devel-ubuntu20.04
  needs: [ "build:x86-64:linux:gcc-10:cuda-11.4" ]
  variables:
    EIGEN_CI_CXX_COMPILER: "g++-10"
    EIGEN_CI_CC_COMPILER: "gcc-10"

# Clang-12, CUDA-11.4
test:x86-64:linux:clang-12:cuda-11.4:
  extends: .test:x86-64:linux:cuda:base
  image: nvidia/cuda:11.4.2-devel-ubuntu20.04
  needs: [ "build:x86-64:linux:clang-12:cuda-11.4" ]
  variables:
    EIGEN_CI_CXX_COMPILER: "clang++-12"
    EIGEN_CI_CC_COMPILER: "clang-12"

##### AArch64 ##################################################################
# GCC-10
.test:aarch64:linux:gcc-10:
  extends: .test:linux:base
  needs: [ "build:aarch64:linux:gcc-10" ]
  variables:
    EIGEN_CI_CXX_COMPILER: "g++-10"
    EIGEN_CI_CC_COMPILER: "gcc-10"
  tags: 
    - eigen-runner
    - linux
    - aarch64

test:aarch64:linux:gcc-10:official:
  extends: .test:aarch64:linux:gcc-10
  allow_failure: true
  variables:
    EIGEN_CI_TEST_TARGET: -L "Official"

test:aarch64:linux:gcc-10:unsupported:
  extends: .test:aarch64:linux:gcc-10
  allow_failure: true
  variables:
    EIGEN_CI_TEST_TARGET: -L "Unsupported"

# Clang 10
.test:aarch64:linux:clang-10:
  extends: .test:linux:base
  needs: [ "build:aarch64:linux:clang-10" ]
  variables:
    EIGEN_CI_CXX_COMPILER: "clang++-10"
    EIGEN_CI_CC_COMPILER: "clang-10"
  tags: 
    - eigen-runner
    - linux
    - aarch64

test:aarch64:linux:clang-10:official:
  extends: .test:aarch64:linux:clang-10
  allow_failure: true
  variables:
    EIGEN_CI_TEST_TARGET: -L "Official"

test:aarch64:linux:clang-10:unsupported:
  extends: .test:aarch64:linux:clang-10
  variables:
    EIGEN_CI_TEST_TARGET: -L "Unsupported"

##### ppc64le ##################################################################
# GCC-10
.test:ppc64le:linux:gcc-10:
  extends: .test:linux:base
  needs: [ "build:ppc64le:linux:gcc-10" ]
  allow_failure: true
  variables:
    EIGEN_CI_CXX_COMPILER: "g++-10"
    EIGEN_CI_CC_COMPILER: "gcc-10"
  tags: 
    - eigen-runner
    - linux
    - ppc64le

test:ppc64le:linux:gcc-10:official:
  extends: .test:ppc64le:linux:gcc-10
  variables:
    EIGEN_CI_TEST_TARGET: -L "Official"

test:ppc64le:linux:gcc-10:unsupported:
  extends: .test:ppc64le:linux:gcc-10
  variables:
    EIGEN_CI_TEST_TARGET: -L "Unsupported"

# Clang 10
.test:ppc64le:linux:clang-10:
  extends: .test:linux:base
  needs: [ "build:ppc64le:linux:clang-10" ]
  allow_failure: true
  variables:
    EIGEN_CI_CXX_COMPILER: "clang++-10"
    EIGEN_CI_CC_COMPILER: "clang-10"
  tags: 
    - eigen-runner
    - linux
    - ppc64le

test:ppc64le:linux:clang-10:official:
  extends: .test:ppc64le:linux:clang-10
  variables:
    EIGEN_CI_TEST_TARGET: -L "Official"

test:ppc64le:linux:clang-10:unsupported:
  extends: .test:ppc64le:linux:clang-10
  variables:
    EIGEN_CI_TEST_TARGET: -L "Unsupported"

##### MSVC #####################################################################
.test:x86-64:windows:base:
  extends: .test:windows:base
  tags: 
    - eigen-runner
    - windows
    - x86-64

# MSVC 14.16 (VS 2017)
.test:x86-64:windows:msvc-14.16:default:
  extends: .test:x86-64:windows:base
  needs: [ "build:x86-64:windows:msvc-14.16:default" ]
  tags: 
    - eigen-runner
    - windows
    - x86-64

test:x86-64:windows:msvc-14.16:default:official:
  extends: .test:x86-64:windows:msvc-14.16:default
  variables:
    EIGEN_CI_TEST_TARGET: -L "Official"

test:x86-64:windows:msvc-14.16:default:unsupported:
  extends: .test:x86-64:windows:msvc-14.16:default
  variables:
    EIGEN_CI_TEST_TARGET: -L "Unsupported"

# MSVC 14.29 (VS 2019)
.test:x86-64:windows:msvc-14.29:default:
  extends: .test:x86-64:windows:base
  needs: [ "build:x86-64:windows:msvc-14.29:default" ]
  tags: 
    - eigen-runner
    - windows
    - x86-64

test:x86-64:windows:msvc-14.29:default:official:
  extends: .test:x86-64:windows:msvc-14.29:default
  variables:
    EIGEN_CI_TEST_TARGET: -L "Official"

test:x86-64:windows:msvc-14.29:default:unsupported:
  extends: .test:x86-64:windows:msvc-14.29:default
  variables:
    EIGEN_CI_TEST_TARGET: -L "Unsupported"

.test:x86-64:windows:msvc-14.29:avx2:
  extends: .test:x86-64:windows:base
  needs: [ "build:x86-64:windows:msvc-14.29:avx2" ]

test:x86-64:windows:msvc-14.29:avx2:official:
  extends: .test:x86-64:windows:msvc-14.29:avx2
  variables:
    EIGEN_CI_TEST_TARGET: -L "Official"

test:x86-64:windows:msvc-14.29:avx2:unsupported:
  extends: .test:x86-64:windows:msvc-14.29:avx2
  variables:
    EIGEN_CI_TEST_TARGET: -L "Unsupported"

.test:x86-64:windows:msvc-14.29:avx512dq:
  extends: .test:x86-64:windows:base
  needs: [ "build:x86-64:windows:msvc-14.29:avx512dq" ]
  tags: 
    - eigen-runner
    - windows
    - x86-64
    - avx512

test:x86-64:windows:msvc-14.29:avx512dq:official:
  extends: .test:x86-64:windows:msvc-14.29:avx512dq
  variables:
    EIGEN_CI_TEST_TARGET: -L "Official"

test:x86-64:windows:msvc-14.29:avx512dq:unsupported:
  extends: .test:x86-64:windows:msvc-14.29:avx512dq
  variables:
    EIGEN_CI_TEST_TARGET: -L "Unsupported"

##### MSVC + CUDA ##############################################################
.test:windows:cuda:base:
  extends: .test:windows:base
  allow_failure: true
  variables:
    EIGEN_CI_TEST_TARGET: -L "gpu"
  tags: 
    - eigen-runner
    - windows
    - x86-64
    - cuda

# MSVC 14.16 + CUDA 9.2
test:x86-64:windows:msvc-14.16:cuda-9.2:
  extends: .test:windows:cuda:base
  needs: [ "build:x86-64:windows:msvc-14.16:cuda-9.2" ]
  
# MSVC 14.29 + CUDA 10.2
test:x86-64:windows:msvc-14.29:cuda-10.2:
  extends: .test:windows:cuda:base
  needs: [ "build:x86-64:windows:msvc-14.29:cuda-10.2" ]
  
# MSVC 14.29 + CUDA 11.4
test:x86-64:windows:msvc-14.29:cuda-11.4:
  extends: .test:windows:cuda:base
  needs: [ "build:x86-64:windows:msvc-14.29:cuda-11.4" ]
